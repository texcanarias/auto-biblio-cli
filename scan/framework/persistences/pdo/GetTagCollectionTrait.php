<?php
declare(strict_types=1); // strict mode

namespace scan\framework\persistences\pdo;

use \PDO;
use \PDOException;
use scan\document\models\{Tag,ArrayTag};

trait GetTagCollectionTrait{

        /**
     * Recupera el listado de tags
     * @param int $id
     * @return ArrayTag
     */
    protected function getTagsCollection(int $id): ArrayTag
    {
        $tags = new ArrayTag();
        try {
            $stmt = $this->dbh->prepare("   SELECT 
                                            id,
                                            name
                                        FROM
                                            tags
                                            INNER JOIN
                                                rel_documents_tags
                                                ON
                                                    tags.id = rel_documents_tags.tags_id 
                                                    AND
                                                    documents_id = :id
                                        ");
            $stmt->bindParam(":id", $id, PDO::PARAM_INT);
            $stmt->execute();
            while ($row = $stmt->fetchObject()) {
                $tags->add(Tag::factoryNew((int)$row->id, $row->name));
            }
        } catch (PDOException $e) {
            throw $e;
        }
        return $tags;
    }


}
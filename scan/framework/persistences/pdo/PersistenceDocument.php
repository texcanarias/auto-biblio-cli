<?php
declare(strict_types=1); // strict mode

namespace scan\framework\persistences\pdo;

use \PDO;
use \PDOException;
use scan\document\models\{Document, Tag, ArrayTag};
use scan\document\persistences\InterfacePersistenceDocument;

/**
 * clase para la persistencia de documentos
 */
class PersistenceDocument extends Persistence implements InterfacePersistenceDocument
{
    use GetTagCollectionTrait;

    public function __construct(Connect $connect)
    {
        parent::__construct($connect);
    }

    /**
     * Registro de un objecto documento
     * @param Document $document
     * @return int Id
     */
    public function saveDataToPersistenceSystem(Document $document): int
    {
        try {

            $stmt = $this->dbh->prepare(" INSERT INTO 
                                             documents 
                                            (name, name_file, mime) 
                                        VALUES
                                            (:name,:nameFile,:mime)");

            try {

                $this->dbh->beginTransaction();
                $name = $document->getName();
                $nameFile = $document->getNameFile();
                $mime = $document->getMime();
                $stmt->bindParam(":name", $name, PDO::PARAM_STR);
                $stmt->bindParam(":nameFile", $nameFile, PDO::PARAM_STR);
                $stmt->bindParam(":mime", $mime, PDO::PARAM_STR);
                $stmt->execute();
                $documentId = $this->dbh->lastInsertId();
                $document->setId((int)$documentId);
                $this->dbh->commit();

                $this->saveTag($document);

                return $document->getId();
            } catch (PDOException $e) {
                $this->dbh->rollback();
                throw $e;
            }
        } catch (PDOException $e) {
            throw $e;
        }
    }

    /**
     * Registro de una etiqueta
     * @param Document $document
     *
     */
    private function saveTag(Document $document)
    {
        foreach ($document->getTags() as $tag) {
            $stmt = $this->dbh->prepare(" SELECT 
                                            id 
                                        FROM 
                                            tags 
                                        WHERE 
                                            name = :name ;");

            $tagName = $tag->getName();
            $stmt->bindParam(":name", $tagName, PDO::PARAM_STR);
            $stmt->execute();
            $resTags = $stmt->fetchAll(PDO::FETCH_OBJ);
            $tagId = null;
            foreach ($resTags as $resTag) {
                $tagId = $resTag->id;
            }
            $isExist = $tagId != null;
            if (!$isExist) {
                try {
                    $this->dbh->beginTransaction();
                    $stmt = $this->dbh->prepare(" INSERT INTO 
                                                     tags 
                                                    (name) 
                                                VALUES
                                                    (:name)");
                    $tagName = $tag->getName();
                    $stmt->bindParam(":name", $tagName, PDO::PARAM_STR);
                    $stmt->execute();
                    $this->dbh->commit();

                    $tagId = $this->dbh->lastInsertId();
                    $document->setTag(Tag::factoryNew((int)$tagId, $tagName));
                } catch (PDOException $e) {
                    $this->dbh->rollback();
                    throw $e;
                }
            }

            //Ahora que está creado, lo relacionamos
            $this->saveRelDocumentTag($document->getId(), (int)$tagId);
        }
    }

    /**
     * Registro de la relacion entre etiqueta y tag
     * @param int $documentId
     * @param int $tagId
     */
    private function saveRelDocumentTag(int $documentId, int $tagId)
    {
        try {
            $this->dbh->beginTransaction();
            $stmt = $this->dbh->prepare(" INSERT INTO 
                                        rel_documents_tags 
                                        (documents_id, tags_id) 
                                    VALUES
                                        (:documents_id, :tags_id)");

            $stmt->bindParam(":documents_id", $documentId, PDO::PARAM_INT);
            $stmt->bindParam(":tags_id", $tagId, PDO::PARAM_INT);
            $stmt->execute();
            $this->dbh->commit();
        } catch (PDOException $e) {
            $this->dbh->rollback();
            throw $e;
        }
    }

    /**
     * Borrado de un dato
     * @param int $id
     * @return Document
     */
    public function deleteDataFromPersistenceSystem(int $id): Document
    {
        try {
            $document = $this->getDataFromPersistenceSystem($id);

            $stmt0 = $this->dbh->prepare(" DELETE FROM 
                                                rel_documents_tags
                                            WHERE 
                                                documents_id = :id");

            $stmt1 = $this->dbh->prepare("   DELETE FROM 
                                                documents 
                                            WHERE
                                                id = :id");

            try {

                $this->dbh->beginTransaction();
                $stmt0->bindParam(":id", $id, PDO::PARAM_INT);
                $stmt0->execute();
                $stmt1->bindParam(":id", $id, PDO::PARAM_INT);
                $stmt1->execute();
                $this->dbh->commit();

                return $document;
            } catch (PDOException $e) {
                $this->dbh->rollback();
                throw $e;
            }
        } catch (PDOException $e) {
            throw $e;
        }
    }

    /**
     * Recuperacion de un documento
     * @param int $id
     * @return null|Document
     */
    public function getDataFromPersistenceSystem(int $id): ?Document
    {
        $document = null;
        try {
            $stmt = $this->dbh->prepare("   SELECT
                                                *
                                            FROM 
                                                documents 
                                            WHERE
                                                id = :id");
            $stmt->bindParam(":id", $id, PDO::PARAM_INT);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_OBJ);

            if($row){
                $TagsCollection = $this->getTagsCollection($id);

                $document = Document::factoryFromArrayTag( (int)$row->id,
                                                        (string)$row->name,
                                                        (string)$row->name_file,
                                                        (string)$row->mime,
                                                        $TagsCollection);
            }
            else{
                $document = Document::factoryFromArray(null,'','','',[]);
            }
            return $document;
        } catch (PDOException $e) {
            $this->dbh->rollback();
            throw $e;
        }
    }
}
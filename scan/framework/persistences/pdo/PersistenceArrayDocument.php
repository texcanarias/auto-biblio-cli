<?php
declare(strict_types=1); // strict mode

namespace scan\framework\persistences\pdo;

use scan\document\models\{ArrayDocument, Document, Tag};
use scan\document\persistences\{InterfacePersistenceArrayDocument,DocumentFilter};


/**
 * Class PersistenceArrayDocument
 * @package scan\framework\persistences\pdo
 */
class PersistenceArrayDocument extends Persistence implements InterfacePersistenceArrayDocument
{

    use GetTagCollectionTrait;

    public function __construct(Connect $connect)
    {
        parent::__construct($connect);
    }

    public function getAll(): ArrayDocument
    {
        $documents = new ArrayDocument();
        try{
            $stmt = $this->dbh->prepare("   SELECT
                                                *
                                            FROM 
                                                documents ");
            $stmt->execute();
            while($row = $stmt->fetchObject()) {
                $document = $this->setNode($row);
                $documents->add($document);
            }

            return $documents;
        } catch (\PDOException $e) {
            $this->dbh->rollback();
            throw $e;
        }
    }

    /**
     * @param string $tag
     * @return ArrayDocument
     * @TODO implementar el SQL de la consulta
     */
    public function getAllFromTag(string $tag): ArrayDocument
    {
        $documents = new ArrayDocument();
        try{
            $stmt = $this->dbh->prepare("   SELECT 
                                                documents.*
                                            FROM
                                                documents
                                                INNER JOIN
                                                    (rel_documents_tags
                                                        INNER JOIN
                                                            tags
                                                            ON
                                                            tags.id = rel_documents_tags.tags_id 
                                                            AND
                                                            tags.name = :tag
                                                        )
                                                    ON
                                                    documents.id = rel_documents_tags.documents_id ");
            $stmt->bindParam(":tag", $tag, \PDO::PARAM_STR);
            $stmt->execute();
            while($row = $stmt->fetchObject()) {
                $document = $this->setNode($row);
                $documents->add($document);
            }

            return $documents;
        } catch (\PDOException $e) {
            $this->dbh->rollback();
            throw $e;
        }
    }

    /**
     * Crea el un objeto de tipo Document para cada registro localizado
     * @param \stdClass $row
     * @return \Document
     */
    private function setNode(\stdClass $row) : \scan\document\models\Document{
        $TagsCollection = $this->getTagsCollection((int)$row->id);
        $document = Document::factoryFromArrayTag((int)$row->id,
                                                $row->name,
                                                $row->name_file,
                                                $row->mime,
                                                $TagsCollection);
        return $document;
    }

    public function setPagination(int $page, int $num) : void{

    }

    public function setFilter(DocumentFilter $documentFilter) : void{

    }
}
<?php
declare(strict_types=1); // strict mode

namespace scan\framework\persistences\pdo;

class Connect{
    private string $host;
    private string $dataBase;
    private string $user;
    private string $pass;
    private string $charset;

    public function __construct(string $host, string $dataBase, string $user, string $pass, string $charset = 'UTF8'){
        $this->host = $host;
        $this->dataBase = $dataBase;
        $this->user = $user;
        $this->pass = $pass;
        $this->charset = $charset;
    }

    public function getHost() : string{
        return $this->host;
    }

    public function getDataBase() : string{
        return $this->dataBase;
    }

    public function getUser() : string{
        return $this->user;
    }

    public function getPass() : string{
        return $this->pass;
    }

    public function getCharset() : string{
        return $this->charset;
    }
    

}
<?php
declare(strict_types=1); // strict mode

namespace scan\framework\persistences\pdo;

use \scan\framework\persistences\pdo\Connect;
use \PDO;
use \PDOException;

class Persistence
{
    private Connect $connect;
    /**
     * Instancia de PDO
     */
    protected PDO $dbh;

    public function __construct(Connect $connect)
    {
        $this->connect = $connect;
        try {
            $dsn = 'mysql:host='.$this->connect->getHost().';dbname='.$this->connect->getDataBase().';charset='.$this->connect->getCharset();
            $user = $this->connect->getUser();
            $pass = $this->connect->getPass();
            $driver_options = [];
            $this->dbh = new PDO($dsn, $user, $pass, $driver_options);
        } catch (PDOException $e) {
            throw $e;
        }
    }


}
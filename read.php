<?php declare (strict_types = 1); // strict mode

require_once __DIR__.'/vendor/autoload.php';

use scan\framework\persistences\pdo\PersistenceArrayDocument;
use scan\framework\persistences\pdo\Connect;



//Hace las funciones de controlador
try{
    $connect = new Connect('db_auto_biblio_cli','auto_biblio','root','root');
    $per = new PersistenceArrayDocument($connect);
    $data = $per->getAll();
    print_r($data);
}
catch(\Exception $ex){
    print_r($ex);
}


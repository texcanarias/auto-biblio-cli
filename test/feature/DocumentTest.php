<?php declare(strict_types=1);

namespace test\unit;

use PHPUnit\Framework\TestCase;
use scan\document\models\Document;
use scan\framework\persistences\pdo\Connect;
use scan\framework\persistences\pdo\PersistenceDocument;

final class DocumentTest extends TestCase
{
    public function testAddOneElement(): void
    {
        $connect = new Connect('db_auto_biblio_cli','auto_biblio','root','root');
        $per = new PersistenceDocument($connect);

        $doc = Document::factoryFromArray(null,"Teoria del todo","teoria.pdf","application/pdf",["fisica","cosmologia"]);
        $this->assertEquals(
            (string)$doc->getName(),
            (string)"Teoria del todo"
        );        

        $documentId = $per->saveDataToPersistenceSystem($doc);
        $this->assertNotEquals(null, $documentId);
    }

    public function testDeleteOneElement(): void
    {
        $connect = new Connect('db_auto_biblio_cli','auto_biblio','root','root');
        $per = new PersistenceDocument($connect);

        $doc = Document::factoryFromArray(null,"Teoria del todo","teoria.pdf","application/pdf",["fisica","cosmologia"]);
        $this->assertEquals(
            (string)$doc->getName(),
            (string)"Teoria del todo"
        );        

        $documentId = $per->saveDataToPersistenceSystem($doc);

        $document = $per->deleteDataFromPersistenceSystem($documentId);

        $this->assertEquals($documentId, $document->getId());
    }


    public function testReadNoExistElement():void{
        $connect = new Connect('db_auto_biblio_cli','auto_biblio','root','root');
        $per = new PersistenceDocument($connect);

        $document = $per->getDataFromPersistenceSystem(99999);
        $this->assertEquals(null, $document->getId());
    }


    public function testDeleteNoExistElement():void{
        $connect = new Connect('db_auto_biblio_cli','auto_biblio','root','root');
        $per = new PersistenceDocument($connect);

        $document = $per->deleteDataFromPersistenceSystem(99999);
        $this->assertEquals(null, $document->getId());
    }
}
<?php declare(strict_types=1);

namespace test\unit;

use PHPUnit\Framework\TestCase;
use scan\document\models\Document;
use scan\framework\persistences\pdo\Connect;
use scan\framework\persistences\pdo\PersistenceArrayDocument;

final class DocumentArrayTest extends TestCase
{
    public function testAll(): void
    {
        $connect = new Connect('db_auto_biblio_cli','auto_biblio','root','root');
        $per = new PersistenceArrayDocument($connect);

        $documents = $per->getAll();
        $exists = 0 < $documents->count();
        $this->assertEquals(true, $exists);
    }

    public function testAllFromTag(): void{
        $connect = new Connect('db_auto_biblio_cli','auto_biblio','root','root');
        $per = new PersistenceArrayDocument($connect);

        $documents = $per->getAllFromTag("fisica");
        $exists = 0 < $documents->count();
        $this->assertEquals(true, $exists);

        $documents = $per->getAllFromTag("ZZZ");
        $exists = 0 == $documents->count();
        $this->assertEquals(true, $exists);
    }
    
}
<?php declare(strict_types=1);

namespace test\unit;

use PHPUnit\Framework\TestCase;
use scan\framework\persistences\pdo\PersistenceArrayDocument;
use scan\framework\persistences\pdo\Connect;

final class PersistenceArrayTest extends TestCase
{
    public function testPersistenceArrayDocument(): void
    {
        $connect = new Connect('db_auto_biblio_cli','auto_biblio','root','root');
        $per = new PersistenceArrayDocument($connect);
        $data = $per->getAll();
        $doc = $data->getListado()[0];


        $this->assertEquals(
            $doc->getName(),
            "Teoria del todo"
        );
    }
}
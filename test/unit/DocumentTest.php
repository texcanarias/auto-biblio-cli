<?php declare(strict_types=1);

namespace test\unit;

use PHPUnit\Framework\TestCase;
use scan\document\models\Document;

final class DocumentTest extends TestCase
{
    public function testFactoryFromArray(): void
    {
        $doc = Document::factoryFromArray(null,"Teoria del todo","teoria.pdf","application/pdf",["fisica","cosmologia"]);
        $this->assertEquals(
            $doc->getName(),
            "Teoria del todo"
        );
    }
}
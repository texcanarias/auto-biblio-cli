<?php declare(strict_types=1);

namespace test\unit\service;

use PHPUnit\Framework\TestCase;
use scan\services\files\Files;

final class FilesTest extends TestCase
{
    public function testMain(): void
    {
        $file = "teoria java ddd.pdf";
        $ge = Files::buildFiles($file);
        $this->assertEquals("pdf", $ge->getExtension());
        $this->assertEquals("application/pdf", $ge->getMime());
        $this->assertEquals(['teoria','java','ddd'], $ge->getTags());
    }
}
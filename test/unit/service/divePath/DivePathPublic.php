<?php declare(strict_types=1);

namespace test\unit\service\divePath;

use scan\document\persistences\InterfacePersistenceDocument;
use scan\services\dive_path\DivePath;

class DivePathPublic extends DivePath{
    private string $pathSourcePublic;

    private function __construct(string $pathSource, string $pathTarget, InterfacePersistenceDocument $PersistenceDocument){
        parent::__construct($pathSource, $pathTarget, $PersistenceDocument);
        $this->pathSourcePublic = $pathSource;
    }

    public static function buildDivePath(string $pathSource, string $pathTarget, InterfacePersistenceDocument $PersistenceDocument) : self{
        return new self($pathSource, $pathTarget, $PersistenceDocument);
    }

    /**
     * Sobrecarga para facilitar el testeo.
     */
    public function processPathPublic() : array{
        try{
            $t = parent::processPath($this->pathSourcePublic);
        }
        catch(\Exception $ex){
            echo "\n";
            echo $ex->getMessage() . "\n";
            echo $ex->getFile() . "\n";
            echo $ex->getLine() . "\n";
        }
        return $this->PathFiles;
    }
}
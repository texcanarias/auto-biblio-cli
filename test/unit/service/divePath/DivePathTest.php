<?php

declare(strict_types=1);

namespace test\unit\service\divePath;

use PHPUnit\Framework\TestCase;

use scan\document\models\Document;
use scan\document\persistences\InterfacePersistenceDocument;
use scan\framework\persistences\pdo\Connect;
use scan\framework\persistences\pdo\PersistenceDocument;

final class DivePathTest extends TestCase
{
    private string $pathSource = "./test/test_doc/NEW/";
    private string $pathTarget = "./test/test_doc/ORDER/";
    public function testMain(): void
    {
        $connect = new Connect('db_auto_biblio_cli','biblio','root','root');
        try {
            $t = DivePathPublic::buildDivePath(
                                                $this->pathSource,
                                                $this->pathTarget,
                                                new PersistenceDocument($connect)
                                            );
            $res = $t->processPathPublic();
            $this->assertEquals("./test/test_doc/NEW/dir/", $res[1]->path);
            $this->assertEquals("vuejs_cookbook.pdf", $res[1]->file);
        } catch (\Exception $ex) {
            echo "\n";
            echo $ex->getMessage() . "\n";
            echo $ex->getFile() . "\n";
            echo $ex->getLine() . "\n";
        }
    }
}
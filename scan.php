<?php declare (strict_types = 1); // strict mode

require_once __DIR__.'/vendor/autoload.php';

use scan\services\dive_path\DivePath;
use scan\framework\persistences\pdo\PersistenceDocument;
use scan\framework\persistences\pdo\Connect;

$pathSource = "../biblio/NEW/";
$pathTarget = "../biblio/ORDER/";


//Hace las funciones de controlador
try{
    $connect = new Connect('db_auto_biblio_cli','auto_biblio','root','root');
    //Servidor
   DivePath::buildDivePath($pathSource, 
                            $pathTarget,   
                            new PersistenceDocument($connect))();
}
catch(\Exception $ex){
    print_r($ex);
}

